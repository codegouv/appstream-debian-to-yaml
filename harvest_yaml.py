#! /usr/bin/env python3


# appstream-debian-to-yaml -- Convert Appstream data from Debian to a Git repository of YAML files
# By: Emmanuel Raviart <emmanuel.raviart@data.gouv.fr>
#
# Copyright (C) 2016 Etalab
# https://git.framasoft.org/codegouv/appstream-debian-to-yaml
#
# appstream-debian-to-yaml is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# appstream-debian-to-yaml is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http:>www.gnu.org/licenses/>.


"""Harvest HTML pages from Harnessing Collaborative Technologies report."""


import argparse
import collections
import gzip
import os
import sys
import urllib.parse
import urllib.request

import yaml


# YAML configuration


class folded_str(str):
    pass


class literal_str(str):
    pass


def dict_constructor(loader, node):
    return collections.OrderedDict(loader.construct_pairs(node))


def dict_representer(dumper, data):
    return dumper.represent_dict(sorted(data.items()))


yaml.add_constructor(yaml.resolver.BaseResolver.DEFAULT_MAPPING_TAG, dict_constructor)

yaml.add_representer(folded_str, lambda dumper, data: dumper.represent_scalar(u'tag:yaml.org,2002:str',
    data, style='>'))
yaml.add_representer(literal_str, lambda dumper, data: dumper.represent_scalar(u'tag:yaml.org,2002:str',
    data, style='|'))
yaml.add_representer(collections.OrderedDict, dict_representer)
yaml.add_representer(str, lambda dumper, data: dumper.represent_scalar(u'tag:yaml.org,2002:str', data))


#


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('yaml_dir', help = 'path of target directory for harvested HTML pages')
    args = parser.parse_args()

    if not os.path.exists(args.yaml_dir):
        os.makedirs(args.yaml_dir)

    for part in ('contrib', 'main', 'non-free'):
        appstream_url = 'https://appstream.debian.org/data/sid/{}/Components-amd64.yml.gz'.format(part)
        response = urllib.request.urlopen(appstream_url)
        yaml_file = gzip.open(response)
        for document in yaml.load_all(yaml_file):
            if document.get('Package') is None:
                continue
            yaml_file_path = os.path.join(args.yaml_dir, '{}.yaml'.format(document['Package']))
            with open(yaml_file_path, 'w', encoding = 'utf-8') as yaml_file:
                yaml.dump(document, yaml_file, allow_unicode=True, default_flow_style=False, indent=2, width=120)

    return 0


if __name__ == '__main__':
    sys.exit(main())
