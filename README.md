# appstream-debian-to-yaml

Convert [Appstream data from Debian](https://appstream.debian.org/data/) to a Git repository of YAML files.

## Usage

```bash
harvest_yaml.py ../appstream-debian-yaml/
```
